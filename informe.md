# 1

## a

El esquema que plantea RPC es la existencia de un proceso "servidor" que responderá a la invocación de ciertos procedimientos. Es así que en los ejemplos se definen dos tipos de proceso: el servidor y el cliente. En el servidor se encuentra la implementación de los procedimientos, y en el cliente se realiza la invocación de dichos procedimientos. Además hay unos cuantos archivos que manejan la comunicación.

Si fueran locales, se podrían poner los procedimientos y sus invocaciones en el mismo archivo que los invoca, unificando cliente y servidor, y prescindir de toda la lógica involucrada con la comunicación de los procesos.

## b

### 1-simple

##### Servidor

```
ejemplosRPC/1-simple$ ./server
Got request: adding 1, 2
Got request: subtracting 1, 2
```

##### Cliente

```
ejemplosRPC/1-simple$ ./client localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

### 2-u1

##### Servidor

No hubo salida.

##### Cliente

```
ejemplosRPC/2-u1$ ./client localhost vagrant
Name vagrant, UID is 1000
vagrant@vagrant-ubuntu-trusty-64:/vagrant/ejemplosRPC/2-u1$ ./client localhost vagran
Name vagran, UID is -1
```

### 3-array

##### Servidor

```
ejemplosRPC/3-array$ ./vadd_service
Got request: adding 10 numbers
```

##### Cliente

```
ejemplosRPC/3-array$ ./vadd_client localhost 1 2 3 4 5 6 7 8 9 10
1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9 + 10 = 55
```

### 4-list

##### Servidor

No hubo salida.

##### Cliente

```
ejemplosRPC/4-list$ ./client localhost 1 2 3 4 5 6
1 2 3 4 5 6
Sum is 21
```

## c

Agregamos una sentencia sleep luego de la línea de creación de la conexión en el cliente. El argumento que recibirá sleep deberá ser pasado como argumento al programa cuando se lo ejecuta. También agregamos la posibilidad de definir el protocolo a utilizar mediante los argumentos del programa. La forma de ejecutar sería:

```
./client PROTOCOL SLEEP_SECONDS HOST N1 N2
```

El resultado es el siguiente:

```
entrega/1c/1-simple$ ./client udp 0 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./client tcp 0 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./client udp 60 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./client tcp 60 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./client udp 120 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./client tcp 120 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

Por lo tanto, se concluye que no hay un tiempo de espera de parte del servidor para con el cliente.

Luego, del lado del servidor, agregamos un sleep y y exit parametrizados.

```
./client SLEEP_SECONDS EXIT
```

Los resultados fueron los siguientes:

```
entrega/1c/1-simple$ ./server 2 0
Got request: adding 1, 2
Got request: subtracting 1, 2
----
entrega/1c/1-simple$ ./client tcp 0 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./server 2 0
Got request: adding 1, 2
Got request: subtracting 1, 2
----
entrega/1c/1-simple$ ./client udp 0 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./server 5 0
Got request: adding 1, 2
----
entrega/1c/1-simple$ ./client tcp 0 localhost 1 2
1 + 2 = 3
Trouble calling remote procedure
```

```
entrega/1c/1-simple$ ./server 5 0
Got request: adding 1, 2
Got request: subtracting 1, 2
----
entrega/1c/1-simple$ ./client udp 0 localhost 1 2
1 + 2 = 3
1 - 2 = -1
```

```
entrega/1c/1-simple$ ./server 30 0
Got request: adding 1, 2
----
entrega/1c/1-simple$ ./client tcp 0 localhost 1 2
Trouble calling remote procedure
```

```
entrega/1c/1-simple$ ./server 30 0
Got request: adding 1, 2
----
entrega/1c/1-simple$ ./client udp 0 localhost 1 2
Trouble calling remote procedure
```

# 4

Operaciones:

* `read(string filename, int pos, int n): {byte[] bytes, int asked, int read_amount}`
* `write(string filename, int n, byte[] buf): int written_amount`